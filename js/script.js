/**
 *  window.onload
 *  Событие onload на window срабатывает, когда загружается вся страница, включая ресурсы на ней – стили, картинки, ифреймы и т.п.
 * 
 *  Используем это событие чтобы избежать ошибки при поиске элемента на странице
 **/

window.onload  = function() {
    console.log('Документ и все ресурсы загружены');
    
    var i;
    for (i = 0; i < 9; i++) {
        document.getElementById('game').innerHTML += '<div class="block space"></div>';
    }
    
    var step = 0;
    document.getElementById('game').onclick = function(event) {
        var symbol = 'x';
        
        if(event.target.className === 'block space'){
            if(step % 2 === 0){
                symbol = 'x';
            } else {
                symbol = 'o';
            }
            event.target.innerHTML = symbol;
            /* Меняем class = "block space" на class = "block" для того чтобы
             * можно было поставить в поле либо крестик, либо нолик без возможности изменения значения */
            event.target.className = 'block';
                        
            //console.log(symbol);

            if(checkWinner(symbol) === false && step === 8){
				symbol = '';
				message(symbol);
			}
			
			step++;
        }
    };
    
    function checkWinner(symbol){
        var allBlocks = document.getElementsByClassName('block');
        //console.log(allBlocks);
        var check = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        var i;
        for (i = 0; i < 8; i++) {
            
            if(allBlocks[check[i][0]].innerHTML === symbol && allBlocks[check[i][1]].innerHTML === symbol && allBlocks[check[i][2]].innerHTML === symbol) {               
                message(symbol);
                break;
            }

        }
		
		return false;
    };
    
    function message(symbol){
        var text;
        (symbol === 'x') ? text = 'Победили крестики!' : text = 'Победили нолики!';
		
		if(symbol === ''){
			text = 'Победила дружба!';
		}
		
        document.getElementById('message').style.display = 'inline-block';
        document.getElementById('message').innerHTML = '<p>' + text + '</p>';
    }
};